// Calculadora
let operaciones = {
	suma: function (a, b) {
		return a + b;
	},
	resta: function (a, b) {
		return a - b;
	},
	multiplicacion: function (a, b) {
		return a * b;
	},
	division: function (a, b) {
		let resultado = 0;
		if(b && b != 0){
			resultado = a / b;
		} else {
			resultado = 'Error!'
		}
		return resultado;
	}
}
// SUBPROB 2. Paso 1.
let op 		   = null
let numResCont = document.querySelector('#numResultado');

// SUBPROB 2. Paso 2.
const setOp = function (operacion) {
	op = operacion;

	// Test unitario de función setOp
	console.log("operacion seleccionada: ", op)
}

const restablecer   = function (){
	console.log("restablecer - Test")

	// Restablecer numResCont
	numResCont.innerText = "0.00"
	op = null

	// Como no tenemos num1 y num2 global entonces seteamos así: 
	document.querySelector('#num1').value = ""
	document.querySelector('#num2').value = ""
}

const resultadoCalc = function () {
	// Voy a obtener num1, num2
	let num1 = document.querySelector('#num1').value;
	let num2 = document.querySelector('#num2').value;

	if(num1 == "" && num2 == ""){
		alert("Num1 y Num2 no deben ser vacios")
		return;
	}

	// Convierto string en numericos
	num1 = parseFloat(num1);
	num2 = parseFloat(num2);

	// Verificar que "op" no es null
	if(op !== null){

		let resultado 		 = operaciones[op](num1, num2);

		// Verifico que el resultado sea un numero
		let tipoResultado 	 = typeof resultado == "number";

		if(tipoResultado){
			numResCont.innerText = resultado;
		} else {
			alert("Error de calculo.")
		}
	} else {

		// Si op === null ~ Quiere decir que no esta seleccionada la operación
		alert("Seleccionar una operaccion correcta.")
	}
}
